ActiveAdmin.register Advices do
  menu priority: 11

  config.filters = false

  permit_params :title, :description

  actions :all, except: [:new, :destroy]

  config.sort_order = 'created_at ASC'

  index download_links: false do
    column :title
    actions
  end

  show do |f|
    attributes_table do
      row :title
      row :description do
        f.description.html_safe
      end
    end
  end

  form do |f|
    f.inputs do
      f.input :title
      f.input :description
      # f.input :description, as: :ckeditor
    end
    f.actions
  end

end
