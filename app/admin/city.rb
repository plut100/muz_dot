ActiveAdmin.register City do
  menu priority: 15

  filter :country

  permit_params :name, :text, :permalink, :seo_title, :seo_description, :seo_text, :image, :country_id, :state

  actions :all, except: [:destroy]

  index download_links: false do
    column :name
    column :country
    column :permalink
    column :created_at
    actions
  end

  show do |f|
    attributes_table do
      row :name
      row :state
      row :country
      row :permalink
      row :image do |city|
        image_tag city.image.url(:thumb)
      end
    end
    attributes_table do
      row :text
      row :seo_text
    end
    attributes_table do
      row :seo_title
      row :seo_description
    end
  end

  form do |f|
    f.inputs do
      f.input :country, as: :select
      f.input :name
      f.input :state
      f.input :text, :input_html => { :rows => 5 }
      f.input :seo_text, :input_html => { :rows => 5 }
      f.input :image, image_preview: true
    end

    f.inputs do
      f.input :seo_title
      f.input :seo_description, :input_html => { :rows => 5 }
    end

    f.actions
  end

end

