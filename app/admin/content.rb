ActiveAdmin.register Content do
  menu priority: 12

  config.filters = false

  permit_params :title, :description, :seo_title, :seo_description

  actions :all, except: [:destroy]

  index download_links: false do
    column :title
    actions
  end

  show do |f|
    attributes_table do
      row :title
      row :description do
        f.description.html_safe
      end
    end
  end

  form do |f|
    f.inputs do
      f.input :title, :input_html => { :disabled => true }
      f.input :description, :input_html => { :rows => 5 }
    end

    f.inputs do
      f.input :seo_title
      f.input :seo_description, :input_html => { :rows => 5 }
    end

    f.actions
  end

end
