ActiveAdmin.register PromoItem do
  menu priority: 7

  permit_params :status, :price, :product_id, :promo_service_id, :view_count

  scope :success
  scope :failure
  scope :processing
  scope :wait_secure
  scope :all, :default => true

  index :download_links => false do
    column :created_at
    column :product

    column :promo_service do |promo|
      if promo.promo_service
        promo.promo_service.title
      end
    end
    column :view_count
    column :price

    column :status do |promo|
      case promo.status
        when "wait_accept"
          status_tag('Подтверждение магазина', :warning)
        when "processing"
          status_tag('Обработка', :warning)
        when "wait_secure"
          status_tag('Проверка', :warning)
        when "sandbox"
          status_tag('Тестовый', :default)
        when "failure"
          status_tag('Ошибка', :error)
        when "success"
          status_tag('Обработано', :ok)
      end
    end

    actions
  end

  show do
    attributes_table do
      row :created_at
      row :product
      row :promo_service
      row :view_count
      row :price
      row :status do |promo|
        case promo.status
          when "wait_accept"
            status_tag('Подтверждение магазина', :warning)
          when "processing"
            status_tag('Обработка', :warning)
          when "wait_secure"
            status_tag('Проверка', :warning)
          when "sandbox"
            status_tag('Тестовый', :default)
          when "failure"
            status_tag('Ошибка', :error)
          when "success"
            status_tag('Обработано', :ok)
        end
      end
    end
  end

  form do |f|
    f.inputs do
      f.input :product, as: :select
      f.input :promo_service, as: :select
      f.input :view_count
      f.input :price
      f.input :status, as: :select, collection: PromoItem.payment_statuses
    end
    f.actions
  end

end
