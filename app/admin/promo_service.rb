ActiveAdmin.register PromoService do
  menu priority: 8

  config.filters = false

  permit_params :title, :view_count, :price
  config.filters = false
  # config.sort_order = 'position'

  index download_links: false do
    column :title
    column :price
    column :created_at
    actions
  end

  show do
    attributes_table do
      row :title
      row :view_count
      row :price
    end
  end

  form do |f|
    f.inputs do
      f.input :title
      f.input :view_count
      f.input :price
    end
    f.actions
  end

end
