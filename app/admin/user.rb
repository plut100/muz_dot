ActiveAdmin.register User do
  menu priority: 2

  permit_params :email, :password, :password_confirmation

  index do
    selectable_column
    id_column
    column :email
    column :current_sign_in_at
    column :sign_in_count
    column :created_at
    actions
  end

  filter :email
  filter :current_sign_in_at
  filter :sign_in_count
  filter :created_at

  form do |f|
    f.inputs "Admin Details" do
      f.input :email
      f.input :password
      f.input :password_confirmation
    end
    f.actions
  end

  show do
    attributes_table do
      row :email
      row :name
      row :surname
      row :avatar do |user|
        image_tag user.avatar
      end
      row :sign_in_count
      row :current_sign_in_at
      row :last_sign_in_at
      row :current_sign_in_ip
      row :last_sign_in_ip
      row :confirmed_at
    end

    panel 'Объявления' do
      table_for user.shop.products do
        column do |product|
          link_to product.title, product
        end
        column do |product|
          product.category.title
        end
        column do |product|
          product.price
        end
      end
    end

    panel 'Соц. сети' do
      table_for user.social_accounts do
        column do |social_account|
          if social_account.provider == "vkontakte"
            link_to social_account.uid, "http://vk.com/id#{social_account.uid}", target: "_blank"
          elsif social_account.provider == "facebook"
            link_to social_account.uid, "http://facebook.com/profile.php?id=#{social_account.uid}", target: "_blank"
          end
        end
        column do |social_account|
          social_account.provider
        end

      end
    end

  end

end
