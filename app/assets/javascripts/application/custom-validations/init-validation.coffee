$ ->
  $.validate
    form: '.js-form-validation'
    modules: 'html5, security'
    validateOnBlur: true
    borderColorOnError: false
