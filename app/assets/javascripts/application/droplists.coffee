$ ->
  $('.dropdown-switch').on 'click', (e)->
    $(@).parents('.dropdown').toggleClass('dropdown_open')
    false
  $('body').on 'click', ->
    $('.dropdown').removeClass('dropdown_open')
  $('body').on 'click', '.dropdown ul.dropdown-list li', ->
    $dropdown = $(@).closest('.dropdown')
    $dropdown.find('.dropdown-switch').html($(@).text())
    $dropdown.removeClass('dropdown_open')
    false unless $dropdown.hasClass('js-allow-default')
  $('#category_list li').on 'click', ->
    $('#product_category_id').val($(@).attr('data-id')).trigger('change');

  $('#countries_list li').on 'click', ->
    $('#shop_country_id').val($(@).attr('data-id')).trigger('change');
    $('#shop_country').val($(@).attr('data-title')).trigger('change');

  $('#condition_list li').on 'click', ->
    $('#product_condition').val($(@).text())

  $('#year_list li').on 'click', ->
    $('#product_year').val($(@).text())

  $('#countries_list_product li').on 'click', ->
    $('#product_country_producing').val($(@).text())

  $('#countries_location_list li').on 'click', ->
    $('#product_country_location').val($(@).text())
    $('#city_loc').show()

  # $('#countries_list li').on 'click', ->
  #   $('#shop_country').val($(@).text())
  #   $('#shop_city').show()
  #   $('#shop_address').show()

  $('#delivery_places_list li').on 'click', ->
    $('#shop_delivery_place').val($(@).text())

  $('.dropdown_error .dropdown ul.dropdown-list li').on 'click', ->
    $(@).parents('.dropdown_error').removeClass('dropdown_error')

  $('.dropdown_error ul.dropdown-list li').on 'click', ->
    $(@).parents('.dropdown_error').removeClass('dropdown_error')

  $('#sort_select li').on 'click', ->
    $('#sort_by').val($(@).attr('data-sort')).trigger('change');

  $('body').on 'change', '.dropdown.has-select select', ->
    $(@).parents('.dropdown').find('.dropdown-switch').html $(@).val()
