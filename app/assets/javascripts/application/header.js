(function() {
  var controlsList;

  controlsList = function() {
    if ($('.control-user').length === 0) {
      return false;
    }
    $('body').on('click', '.control-user', function() {
      if ($('.user-menu').hasClass('visible')) {
        $('.user-menu').removeClass('visible');
      } else {
        $('.user-menu').addClass('visible');
      }
      return false;
    });
    /*$('body').on('click', '.user-menu a', function(e) {
      return e.stopPropagation();
    });*/
    return $('body').on('click', '*:not(.control-user, .control-user *, .user-menu a)', function() {
      if ($('.user-menu').hasClass('visible')) {
        $('.user-menu').removeClass('visible');
      }
      return true;
    });
  };

  $(function() {
    return controlsList();
  });

}).call(this);
