$ ->
  $('#login_button').on 'click', ->
    $('body').addClass('body-popup_open');
    $('.popup-overlay').show()
    $('#login_popup').css('display', 'block')
    false

  $('.popup__close').on 'click', ->
    $('.popup-overlay').hide()
    $('body').removeClass('body-popup_open');
    $('#login_popup').css('display', 'none')
    false


  $('#login_button_two').on 'click', ->
    $('.popup-overlay').show()
    $('body').addClass('body-popup_open');
    $('#login_popup').css('display', 'block')
    false

  $('#login_button_three').on 'click', ->
    $('.popup-overlay').show()
    $('body').addClass('body-popup_open');
    $('#login_popup').css('display', 'block')
    false

  $('#sale_button').on 'click', ->
    $('.popup-overlay').show()
    $('body').addClass('body-popup_open');
    $('#email_blank').css('display', 'block')
    false

  $('.popup__close').on 'click', ->
    $('body').removeClass('body-popup_open');
    $('.popup-overlay').hide()
    $('#email_blank').css('display', 'none')
    false

  $('#register_button').on 'click', ->
    $('.popup-overlay').show()
    $('body').addClass('body-popup_open');
    $('#register_popup').css('display', 'block')
    false

  $('.popup__close').on 'click', ->
    $('body').removeClass('body-popup_open');
    $('.popup-overlay').hide()
    $('#register_popup').css('display', 'none')
    false

  $('#recovery_button a').on 'click', ->
    $('#login_popup').css('display', 'none')
    $('#recovery_popup').css('display', 'block')
    false

  $('.popup__close').on 'click', ->
    $('.popup-overlay').hide()
    $('body').removeClass('body-popup_open');
    $('#recovery_popup').css('display', 'none')
    false

  $('.popup__close').on 'click', ->
    $('.popup-overlay').hide()
    $('body').removeClass('body-popup_open');
    $('#subscribe_popup').css('display', 'none')
    false

  $('#feedback_button').on 'click', ->
    $('body').addClass('body-popup_open');
    $('.popup-overlay').show()
    $('#feedback_popup').css('display', 'block')
    false

  $('#feedback_button_alt').on 'click', ->
    $('body').addClass('body-popup_open');
    $('.popup-overlay').show()
    $('#feedback_popup').css('display', 'block')
    false

  $('.popup__close').on 'click', ->
    $('.popup-overlay').hide()
    $('body').removeClass('body-popup_open');
    $(this).closest('.popup_open').css('display', 'none')
    false

  $('#change_password').on 'click', ->
    $('body').addClass('body-popup_open');
    $('.popup-overlay').show()
    $('#password_edit_popup').css('display', 'block')
    false

  $('.popup__close').on 'click', ->
    $('.popup-overlay').hide()
    $('body').removeClass('body-popup_open');
    $('#password_edit_popup').css('display', 'none')
    false

  $('#register_popup_button').on 'click', ->
    $('body').addClass('body-popup_open');
    $('#register_popup').css('display', 'block')
    $('#login_popup').hide()
    false

  $('#register_popup_button_two').on 'click', ->
    $('body').addClass('body-popup_open');
    $('#register_popup').css('display', 'block')
    $('#recovery_popup').hide()
    false

  $('#login_popup_button').on 'click', ->
    $('body').addClass('body-popup_open');
    $('#login_popup').css('display', 'block')
    $('#register_popup').hide()
    false

  $('#recovery_popup_back_button').on 'click', ->
    $('body').addClass('body-popup_open');
    $('#login_popup').css('display', 'block')
    $('#recovery_popup').hide()
    false

  $('#flash__close').on 'click', ->
    $('.flash-message').fadeOut(500)

  $('#flash__close_new_user_no').on 'click', ->
    $('#flash__open__new_user').fadeOut(500)
