$ ->
  $('body').on 'click', '#pick_details', ->
    $('#detail').show()
    $('#product_detail_id').val ''
    $('#detail_name').text 'Выберите принадлежность'
    $.ajax(
      url: '/products/get_details'
      method: "POST"
      data: {category_id: $('#product_category_id').val()}
      ).done (data)->
      $('#detail_drop').html(data)
      return

  $('body').on 'click', '#detail_drop li', ->
    $('#product_detail_id').val($(@).attr('data-id'))
