$ ->
  $('body').on 'click', '.delete_product', ->
    product_id = $(this).attr('data-id')
    $('.popup-overlay').show()
    $('body').addClass('body-popup_open');
    $('#delet_popup').show()
    $('#delet_popup form').attr('action', '/products/' + product_id)
    false

  $('.delete_abort').on 'click', ->
    $('.popup-overlay').hide()
    $('body').removeClass('body-popup_open');
    $('#delet_popup').hide()
    false

  $('body').on 'change', '.reason-button input', (e) ->
    $(@).closest('form').submit();
