$ ->
  $('#product_category_id').on 'change', ->
    $('#product_subcategory_id').val ''
    $('#product_detail_id').val ''
    $('#detail').hide()

    if $(@).val() is '16' # Return false if chosen category_id is 16 with name 'Прочее'
      $('#subcat').hide()
      return false

    $('#subcat').show()
    $('#subcat_name').text 'Выберите субкатегорию'

    $.ajax(
      url: '/products/get_subcategories'
      method: "POST"
      data: {category_id: $('#product_category_id').val()}
      ).done (data)->
      $('#subcat_drop').html(data)
      return

  $('body').on 'click', '#subcat_drop li', ->
    $('#product_subcategory_id').val $(@).attr('data-id')

    unless $(@).prop('id') is 'pick_details'
      $('#product_detail_id').val ''
      $('#detail').hide()
