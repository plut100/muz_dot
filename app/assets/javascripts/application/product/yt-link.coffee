ytLink =->
  if $('iframe').length == 0
    return

  videoLink = $('iframe').attr('src')
  videoLink = videoLink.replace('https://www.youtube.com/watch?v=', 'https://www.youtube.com/embed/')
  $('iframe').attr('src', videoLink)


$(document).ready ->
  ytLink()

$ ->
  $('.field_with_errors input').keydown ->
      $(@).parents('.inputbox').find('.inputbox__error').remove()
