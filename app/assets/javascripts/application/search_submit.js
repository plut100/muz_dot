$(document).ready(function () {
  $('#submit_q').click(function(){
      $('#products_q').submit();
      return false;
  });

  $('body').on('click', '#submit_search', function(e) {
    e.preventDefault()
    $(this).closest('form').submit();
  });

  $('body').on('submit', '#products_search', function() {
    var $filterForm = $('#filter_form');
    var $searchParam = $('#search_param');
    if ($filterForm.length) {
      var searchVal = $('#search').val();

      if ($searchParam.length) {
        $searchParam.val(searchVal);
        $filterForm.submit();
      } else {
        var $input = $('<input>').attr({
                                type: 'hidden',
                                value: searchVal,
                                name: 'search'});
        $filterForm.prepend($input).submit();
      }
      return false
    } else {
      return true;
    }
  });
})
