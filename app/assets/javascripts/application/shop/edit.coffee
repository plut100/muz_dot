$ ->
  if $('.form_shop').length == 0
    return false
  $('#cover_button').on 'click', ->
    $('#shop_cover').trigger 'click'
    false
  $('#submit').on 'click', (e)->
    $('.form_shop').submit()
    false
  true

  $('.field_with_errors input').keydown ->
    $(@).parents('.inputbox').find('.inputbox__error').remove()

  $('#shop_cover').on 'change', ->
    formData = new FormData()
    fileType = $(@)[0].files[0].type
    fileSize = $(@)[0].files[0].size
    allowdtypes = $(@).prop('accept')

    if allowdtypes.indexOf(fileType) < 0
      alert 'Недопустимый формат изображения'
      $(@).val ''
      return false

    if fileSize > (5 * 1024 * 1024)
      alert 'Размер изображения не должен превышать 5Мб'
      $(@).val ''
      return false

    formData.append 'shop_cover', $(@)[0].files[0]
    $.ajax
      type: 'post'
      url: '/shops/update_shop_cover'
      data: formData
      contentType: false
      processData: false
    .done (resp) ->
      $(@).val ''
      $('.main-pic').css background: "url(#{resp})"
    .fail (resp) ->
      $(@).val ''
      alert 'Чтото пошло не так, попробуйте повторить чуть позже'
    .always (resp) ->
      $(@).val ''
