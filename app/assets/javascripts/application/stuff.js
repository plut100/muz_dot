$( document ).ready(function() {
  if($('.js-masonry').length > 0){
    var container = document.querySelector('.js-masonry');
    imagesLoaded( container, function() {
      var msnry = new Masonry(container, {
        itemSelector: '.brick-item',
        columnWidth: 220,
        gutter: 20
    });
    });
  }

  if($('.js-masonry-2').length > 0){
    var container = document.querySelector('.js-masonry-promo');
    imagesLoaded( container, function() {
      var msnry = new Masonry(container, {
        itemSelector: 'item_promo',
        columnWidth: 220,
        gutter: 20
    });
    });
  }

  $(function() {
      $('[data-toggle="tooltip"]').tooltip()
  })
})