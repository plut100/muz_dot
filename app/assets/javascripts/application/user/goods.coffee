$ ->
  if $('.promo-button-js').length == 0
    return false
  
  $('.promo-button-js').hover (->
    $('.popover-content').slideUp(200)
    $(@).parent().find('.popover-content').slideDown(200)
    return
  ), ->
    return

  $('.popover-content').on 'mouseleave', ->
    setTimeout (->
      $('.popover-content').slideUp(200)
      return
    ), 200

  $('.js_status_ckeckbox').on 'change', ->
    $(@).closest("form").submit()
