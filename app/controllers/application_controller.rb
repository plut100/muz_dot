class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_filter :set_new_message_count
  before_filter :set_feedbacks
  before_filter :set_flash

  def set_new_message_count
    @new_messages = current_user.count_all_unread_messages if current_user
  end

  def set_feedbacks
    @footer_subscriber = Subscriber.new
    @footer_feedback = Feedback.new
  end

  def set_flash
    flash[:notice] = ''
  end


  def set_seo(page)
    set_meta_tags :site => '', :title => page.seo_title ? page.seo_title : '', :reverse => true,
                  :description => page.seo_description ? page.seo_description : ""
  end
end
