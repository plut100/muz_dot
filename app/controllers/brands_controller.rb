class BrandsController < ApplicationController
  def index
    if params[:alpha]
      @brands = Brand.approved.alpha(params[:alpha]).with_products.order('brands.title ASC')
    elsif params[:digit]
      @brands = Brand.order('title ASC').approved.where("title >= '0' and title < ':'")
    else
      @brands = Brand.order('title ASC').approved.where(id: Product.all.map(&:brand_id).uniq)
    end
    @content = Content.find_by(:title => "brands")
    set_seo(@content)
  end

  def show
    @brand = Brand.friendly.find(params[:id])

    @categories = Category.all
    @brands = Brand.approved.find_by_sql("SELECT brands.*, COUNT(products.id) AS c FROM brands, products WHERE products.brand_id = brands.id GROUP BY brands.id ORDER BY c DESC").take(12)

    prepare_products

    if params[:category].present?
      @promo = @brand.products.joins(:promo_items).where('promo_items.status = ? AND promo_items.view_count > ?', 'success', 0).where(:category_id => params[:category]).sample(3)
    else
      @promo = @brand.products.joins(:promo_items).where('promo_items.status = ? AND promo_items.view_count > ?', 'success', 0).where(:brand_id => @brand).sample(3)
    end

    set_seo(@brand)
  end

  def prepare_products
    limit = 16

    @page = params[:page] ? params[:page].to_i : 1
    if params[:search]
      relation = @brand.products.search(params[:search])
    else
      relation = @brand.products.all
    end

    unless params[:category].blank?
      relation = relation.where(:category_id => params[:category])
    end
    unless params[:subcategory].blank?
      relation = relation.where(:subcategory_id => params[:subcategory])
    end
    unless params[:detail].blank?
      relation = relation.where(:detail_id => params[:detail])
    end
    unless params[:brands].blank?
      relation = relation.where(:brand_id => params[:brands])
    end
    unless params[:min_year].blank?
      relation = relation.where('year >= ?', params[:min_year].to_i)
    end
    unless params[:max_year].blank?
      relation = relation.where('year <= ?', params[:max_year].to_i)
    end
    if params[:main_filter] == "handmade"
      relation = relation.where('condition != ? OR handmade = ?', "Новое", true)
    end
    if params[:main_filter] == "new"
      relation = relation.where(:condition => "Новое")
    end
    @products = relation.limit(limit).offset((@page - 1)*limit)
    count = relation.count
    @pages = count.modulo(limit) == 0 ? count/limit : count.div(limit) + 1
  end
end
