class CategoriesController < ApplicationController

  def show
    @category = Category.friendly.find(params[:id])

    @categories = Category.all
    prepare_products
    @brands = Brand.where(id: @category.products.pluck(:brand_id)).take(12)

    if params[:category].present?
      @promo = @category.products.joins(:promo_items).where('promo_items.status = ? AND promo_items.view_count > ?', 'success', 0).sample(3)
    elsif params[:subcategory].present?
      @promo = @category.products.joins(:promo_items).where('promo_items.status = ? AND promo_items.view_count > ?', 'success', 0).where(:subcategory_id => params[:subcategory]).sample(3)
    else
      @promo = @category.products.joins(:promo_items).where('promo_items.status = ? AND promo_items.view_count > ?', 'success', 0).sample(3)
    end

    set_seo(@category)
  end

  def prepare_products
    limit = 25
    @page = params[:page] ? params[:page].to_i : 1
    if params[:search]
      relation = @category.products.search(params[:search])
    else
      relation = @category.products.all
    end
    unless params[:detail].blank?
      relation = relation.where(:detail_id => params[:detail])
    end
    unless params[:brands].blank?
      relation = relation.where(:brand_id => params[:brands])
    end
    unless params[:min_year].blank?
      relation = relation.where('year >= ?', params[:min_year].to_i)
    end
    unless params[:max_year].blank?
      relation = relation.where('year <= ?', params[:max_year].to_i)
    end
    if params[:main_filter] == "handmade"
      relation = relation.where('condition != ? OR handmade = ?', "Новое", true)
    end
    if params[:main_filter] == "new"
      relation = relation.where(:condition => "Новое")
    end
    @products = relation.limit(limit).offset((@page - 1)*limit)
    count = relation.count
    @pages = count.modulo(limit) == 0 ? count/limit : count.div(limit) + 1
  end

end
