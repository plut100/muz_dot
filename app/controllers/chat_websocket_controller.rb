class ChatWebsocketController < WebsocketRails::BaseController
  def initialize_session
  end

  def new_message
    @message = Message.create(
      conversation_id: message[:conversation_id],
      sender: current_user,
      text: message[:text]
    )
    WebsocketRails[@message.conversation.id.to_s].trigger :message_added, view_context.render(partial: 'conversations/message', locals: {message: @message}, layout: false)
  end

end