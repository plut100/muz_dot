class ConversationsController < ApplicationController
  def index
    @conversations = Conversation.where('sender_id = ? OR recipient_id = ?', current_user.id, current_user.id)
    @conversations = @conversations.where('(sender_id = ? AND show_for_sender = ?) OR (recipient_id = ? AND show_for_recipient = ?)', current_user.id, true, current_user.id, true)

    if params[:sentence].present?
      ids = @conversations.pluck(:id)
      cids = Message.where(conversation_id: ids).where('messages.text ILIKE ?', '%' + params[:sentence] + '%').pluck(:conversation_id)
      @conversations = @conversations.where(id: cids)
    end
  end

  def create
    @conversation = Conversation.find_or_create(current_user.id, params[:user_id])
    redirect_to @conversation
  end

  def show
    @conversation = Conversation.find(params[:id])
    gon.channel = @conversation.id.to_s
    @conversation.update_check(current_user)

    if Rails.env.staging?
      gon.websocketpath = 'muzdot.thedigitalcrafters.com/websocket'
    elsif Rails.env.production?
      gon.websocketpath = 'im.muztochka.com/websocket'
    elsif Rails.env.development?
      gon.websocketpath = 'localhost:3000/websocket'
    end
  end

  def destroy
    @conversation = Conversation.find(params[:id])
    if @conversation.sender_id == current_user.id
      @conversation.update(show_for_sender: false)
    elsif @conversation.recipient_id == current_user.id
      @conversation.update(show_for_recipient: false)
    end
    redirect_to conversations_path
  end
end
