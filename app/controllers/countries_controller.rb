class CountriesController < ApplicationController

  def index
    @countries = Product.reorder('country DESC').uniq.pluck(:country)
    @content = Content.find_by(:title => "countries")
    set_seo(@content)
  end

  def show
    @geo = Country.find_by(:permalink => params[:id])

    @products = Product.where(country: @geo.name)

    @categories = Category.all
    @brands = Brand.approved.find_by_sql("SELECT brands.*, COUNT(products.id) AS c FROM brands, products WHERE products.brand_id = brands.id GROUP BY brands.id ORDER BY c DESC").take(12)

    prepare_products

    if params[:category].present?
      @promo = @products.joins(:promo_items).where('promo_items.status = ? AND promo_items.view_count > ?', 'success', 0).where(:category_id => params[:category]).sample(3)
    else
      @promo = @products.joins(:promo_items).where('promo_items.status = ? AND promo_items.view_count > ?', 'success', 0).where(country: @geo.name).sample(3)
    end

    set_seo(@geo)
  end


  def prepare_products
    limit = 16

    @page = params[:page] ? params[:page].to_i : 1
    if params[:search]
      relation = @products.where(country: @geo.name).search(params[:search])
    else
      relation = @products.where(country: @geo.name)
    end

    unless params[:category].blank?
      relation = relation.where(:category_id => params[:category])
    end
    unless params[:subcategory].blank?
      relation = relation.where(:subcategory_id => params[:subcategory])
    end
    unless params[:detail].blank?
      relation = relation.where(:detail_id => params[:detail])
    end
    unless params[:brands].blank?
      relation = relation.where(:brand_id => params[:brands])
    end
    unless params[:min_year].blank?
      relation = relation.where('year >= ?', params[:min_year].to_i)
    end
    unless params[:max_year].blank?
      relation = relation.where('year <= ?', params[:max_year].to_i)
    end
    if params[:main_filter] == "handmade"
      relation = relation.where('condition != ? OR handmade = ?', "Новое", true)
    end
    if params[:main_filter] == "new"
      relation = relation.where(:condition => "Новое")
    end
    @products = relation.where(country: @geo.name).limit(limit).offset((@page - 1)*limit)
    count = relation.where(country: @geo.name).count
    @pages = count.modulo(limit) == 0 ? count/limit : count.div(limit) + 1
  end
end
