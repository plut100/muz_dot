class HomeController < ApplicationController
  def index
    if params[:set_password].present? && current_user.present?
      @user = current_user
    end

    @categories = Category.all
    @brands = Brand.approved.find_by_sql("SELECT brands.*, COUNT(products.id) AS c FROM brands, products WHERE products.brand_id = brands.id GROUP BY brands.id ORDER BY c DESC").take(12)
    prepare_products

    if params[:category].present?
      @promo = Product.joins(:promo_items).where('promo_items.status = ? AND promo_items.view_count > ?', 'success', 0).where(:category_id => params[:category]).sample(3)
    elsif params[:subcategory].present?
      @promo = Product.joins(:promo_items).where('promo_items.status = ? AND promo_items.view_count > ?', 'success', 0).where(:subcategory_id => params[:subcategory]).sample(3)
    else
      @promo = Product.joins(:promo_items).where('promo_items.status = ? AND promo_items.view_count > ?', 'success', 0).sample(3)
    end

    @content = Content.find_by(:title => "home")
    set_seo(@content)

    if params[:page] == '1'
      redirect_to root_path, :status => 301
    end
  end

  def prepare_products
    limit = 25
    @page = params[:page] ? params[:page].to_i : 1
    if params[:search]
      relation = Product.search(params[:search])
    else
      relation = Product.all
    end
    unless params[:category].blank?
      relation = relation.where(:category_id => params[:category])
    end
    unless params[:subcategory].blank?
      relation = relation.where(:subcategory_id => params[:subcategory])
    end
    unless params[:detail].blank?
      relation = relation.where(:detail_id => params[:detail])
    end
    unless params[:brands].blank?
      relation = relation.where(:brand_id => params[:brands])
    end
    unless params[:min_year].blank?
      relation = relation.where('year >= ?', params[:min_year].to_i)
    end
    unless params[:max_year].blank?
      relation = relation.where('year <= ?', params[:max_year].to_i)
    end
    if params[:main_filter] == "handmade"
      relation = relation.where('condition != ? OR handmade = ?', "Новое", true)
    end
    if params[:main_filter] == "new"
      relation = relation.where(:condition => "Новое")
    end
    if params[:sort_by] == 'newest'
      relation = relation.reorder('created_at DESC')
    elsif params[:sort_by] == 'price_low_first'
      relation = relation.reorder('price ASC')
    elsif params[:sort_by] == 'price_high_first'
      relation = relation.reorder('price DESC')
    end
    relation = relation.where('price <= ?', params[:max_price]) if params[:max_price].present?
    @products = relation.limit(limit).offset((@page - 1)*limit)
    count = relation.count
    @pages = count.modulo(limit) == 0 ? count/limit : count.div(limit) + 1
  end

end
