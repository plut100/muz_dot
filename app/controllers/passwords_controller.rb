class PasswordsController <  Devise::PasswordsController
  respond_to :js
  layout false
  # POST /resource/password
  def create
    self.resource = resource_class.send_reset_password_instructions(resource_params)
    yield resource if block_given?

    if successfully_sent?(resource)
      respond_with({}, location: after_sending_reset_password_instructions_path_for(resource_name))
    else
      # respond_with(resource)
      render 'error'
    end
  end

  def edit
    token = Devise.token_generator.digest(self, :reset_password_token, params[:reset_password_token])
    self.resource = resource_class.find_by_reset_password_token(token)

    if sign_in(resource)
      respond_with_navigational(resource){ redirect_to root_path(set_password: DateTime.now.to_i) }
    else
      render 'error'
    end
  end
end
