class PaymentsController < ApplicationController

  skip_before_action :verify_authenticity_token
  protect_from_forgery except: :notify

  def notify
    params.permit!
    @promo_item = PromoItem.find(params[:order_id])
    @promo_item.update_attribute(:status, params[:status])
  end

  private

  def order_attrs
    params.require(:promo_item).permit(:status)
  end

end
