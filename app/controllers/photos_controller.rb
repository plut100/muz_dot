class PhotosController < ApplicationController

  def create
    respond_to do |format|
      @photos = []
      photo_params['image'].each do |image|
        photo = Photo.new(:image => image)
        if photo.save
          @photos << photo
        end
      end
      if @photos.length > 0
        format.js {render 'success', layout: false}
      else
        format.js { render 'error', layout: false }
      end
    end
  end

  private

  def photo_params
    params.require(:photo).permit!
  end
end
