class ProductsController < ApplicationController
  before_action :authenticate_user!, only: [:new]
  before_action :increment_views_counter, :only => :show
  before_action :set_cookie, only: :show
  before_action :check_user, :only => [:edit, :update, :destroy]

  def prepare_form
    gon.push({:brands => Brand.where(:approved => true).all.map(&:title)})
    @categories = Category.all
    @countries = ["Украина", "Россия", "Беларусь", "Казахстан", "Азербайджан", "Армения", "Грузия", "Израиль", "США", "Германия", "Латвия", "Литва", "Андорра", "Афганистан", "Албания", "Ангола", "Аргентина", "Австрия", "Австралия", "Азербайджан", "Босния и Герцеговина", "Барбадос", "Бангладеш", "Бельгия", "Буркина-Фасо", "Болгария", "Бахрейн", "Бурунди", "Бенин", "Сен-Бартелеми", "Боливия", "Бразилия", "Багамские Острова", "Бутан", "Ботсвана", "Белиз", "Канада", "Демократическая Республика Конго", "Центральноафриканская Республика", "Республика Конго", "Швейцария", "Кот-д’Ивуар", "Чили", "Камерун", "Китай", "Колумбия", "Коста-Рика", "Куба", "Кабо-Верде", "Кипр", "Чехия", "Джибути", "Дания", "Доминика", "Доминиканская Республика", "Алжир", "Эквадор", "Эстония", "Египет", "Западная Сахара", "Эритрея", "Испания", "Эфиопия", "Финляндия", "Франция", "Габон", "Великобритания", "Гренада", "Гана", "Гамбия", "Гвинея", "Экваториальная Гвинея", "Греция", "Гватемала", "Гвинея-Бисау", "Гайана", "Гондурас", "Хорватия", "Республика Гаити", "Венгрия", "Индонезия", "Ирландия", "Индия", "Ирак", "Иран", "Исландия", "Италия", "Джерси", "Иордания", "Япония", "Кения", "Киргизия", "Камбоджа", "Коморы", "Сент-Китс и Невис", "КНДР", "Республика Корея", "Кувейт", "Лаос", "Ливан", "Сент-Люсия", "Лихтенштейн", "Шри-Ланка", "Либерия", "Лесото", "Люксембург", "Ливия", "Марокко", "Монако", "Молдова", "Черногория", "Мадагаскар", "Македония", "Мали", "Мьянма", "Монголия", "Мавритания", "Мальта", "Маврикий", "Мальдивы", "Малави", "Мексика", "Малайзия", "Мозамбик", "Намибия", "Нигер", "Нигерия", "Никарагуа", "Нидерланды", "Норвегия", "Непал", "Новая Зеландия", "Оман", "Панама", "Перу", "Папуа — Новая Гвинея", "Филиппины", "Пакистан", "Польша", "Пуэрто-Рико", "Палестина", "Португалия", "Палау", "Парагвай", "Катар", "Румыния", "Сербия", "Руанда", "Саудовская Аравия", "Соломоновы Острова", "Сейшельские Острова", "Судан", "Швеция", "Сингапур", "Словения", "Словакия", "Сьерра-Леоне", "Сан-Марино", "Сенегал", "Сомали", "Суринам", "Южный Судан", "Сан-Томе и Принсипи", "Сальвадор", "Сирия", "Свазиленд", "Чад", "Того", "Таиланд", "Таджикистан", "Восточный Тимор", "Туркмения", "Тунис", "Турция", "Тринидад и Тобаго", "Танзания", "Уганда", "Уругвай", "Узбекистан", "Ватикан", "Сент-Винсент и Гренадины", "Венесуэла", "Вьетнам", "Вануату", "Йемен", "ЮАР", "Замбия", "Зимбабве"]
    @advices = Advices.all
    gon.producing_countries = @countries
  end

  def show
    @product = Product.unscoped.friendly.find(params[:id])
#    if @product.shop.user != current_user && @product.status == false
#      raise ActionController::RoutingError.new('Not Found')
#    else
      @product.punch(request)
      @product.promo_check
      @similar_products = Product.where('id != ?', @product.id).limit(6).order("RANDOM()")
#    end

    @product_deletion_reasons = ProductDeletionReason.all
    set_seo(@product)
  end

  def edit
    prepare_form
    @product = Product.unscoped.friendly.find(params[:id])
    unless @product.shop.user == current_user
      render :status => 404
    end
  end

  def new
    @product = Product.new(:country => current_user.shop.country, :city => current_user.shop.city, :state => current_user.shop.state, :formatted_address => current_user.shop.formatted_address)
    prepare_form
  end

  def create
    @product = Product.new(product_params)
    @product.shop = current_user.shop
    unless params['product']['brand'].blank?
      @product.brand = Brand.find_or_create_by(:title => params['product']['brand'])
    end
    @product.validate_photos(params[:product][:photos])
    respond_to do |format|
      if @product.save
        @product.get_photos(params[:product][:photos])
        format.html { redirect_to @product }
        # format.js { render 'success', layout: false }
      else
        prepare_form
        puts @product.errors.full_messages
        format.html { render action: 'new' }
        format.json { render json: @product.errors }
      end
    end
  end

  def update
    @product = current_user.shop.products.unscoped.friendly.find(params[:id]) rescue nil
    if params[:product] && params[:product]['brand']
      @product.brand = Brand.find_or_create_by(:title => params['product']['brand'])
    end
    if params[:product] && params[:product][:photos]
      @product.validate_photos(params[:product][:photos])
      @product.get_photos(params[:product][:photos])
    end
    respond_to do |format|
      puts 'here'
      if @product.errors.empty? && @product.update(product_params) && !product_params.has_key?(:status)
        format.html { redirect_to goods_user_path(current_user) }
        format.js { render 'success', layout: false }
      elsif product_params.has_key?(:status)
        if params[:product]['status']
          @product.update_attribute(:permission, 'non_active') if params[:product]['status'] == '0'
          @product.update_attribute(:permission, 'active') if params[:product]['status'] == '1'
          format.html { redirect_to goods_user_path(current_user) }
          format.js { render 'success', layout: false }
        end
      else
        prepare_form
        format.html { render action: 'edit' }
        format.json { render json: @product.errors }
      end
    end
  end

  def destroy
    @product = Product.unscoped.friendly.find(params[:id])
    if @product.shop.id == current_user.shop.id
      log = ProductDeletionLog.new(user: current_user,
                                   reason: ProductDeletionReason.find(params[:reason_id]),
                                   product: @product
      )

      if log.save
        @product.permission = 'deleted'
        @product.save(validate: false)
      end

      respond_to do |format|
        format.html { redirect_to goods_user_path(current_user), :flash => { notice: "Товар удален" } }
      end
    end
  end

  def get_subcategories
    @subcats = Subcategory.where(:category => params[:category_id])
    respond_to do |format|
      format.html{ render 'get_subcategories' ,layout: false }
    end
  end

  def get_details
    @details = Detail.where(:category => params[:category_id])
    respond_to do |format|
      format.html{ render 'get_details' ,layout: false }
    end
  end

  private

  def product_params
    params.require(:product).permit(:shop_id, :category_id, :subcategory_id, :detail_id, :title, :description, :mark, :year, :color, :price, :condition,
                                    :country_producing, :formatted_address, :country, :state, :city, :lat, :lng, :handmade, :status, :image, :video)
  end

  def check_user
    @product = Product.unscoped.friendly.find(params[:id])
    if @product.shop.user != current_user
      redirect_to root_path
    end
  end

  def increment_views_counter
    @product = Product.unscoped.friendly.find(params[:id])
    Protector.insecurely do
      impressionist @product
    end
  end

  def set_cookie
    product = Product.unscoped.friendly.find(params[:id])
    if cookies["muztochka_pr_#{product.id}"].nil?
      cookies["muztochka_pr_#{product.id}"] = { value: product.id, expires: 24.hours.from_now }
      product.visitors_count.create(remote_ip: request.remote_ip)
    end
  end
end
