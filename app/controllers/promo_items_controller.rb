class PromoItemsController < ApplicationController
  skip_before_action :verify_authenticity_token

  def create
    @promo_item = PromoItem.new(promo_item_params)
    @promo_item.price = @promo_item.promo_service.price
    @promo_item.view_count = @promo_item.promo_service.view_count
    @promo_item.status = 'pending'

    respond_to do |format|
      if @promo_item.save
        format.html { redirect_to @promo_item }
      else
        format.html { redirect_to :back }
      end
    end
  end

  def show
    @promo_item = PromoItem.find(params[:id])

    @liqpay_request = Liqpay::Request.new(
      :amount => @promo_item.price,
      :currency => 'USD',
      :order_id => @promo_item.id,
      :description => "Оплата за рекламу #{@promo_item.product.title}, #{@promo_item.view_count} пользовательских кликов",
      :result_url => "https://muztochka.com/promo_items/#{@promo_item.id}/notify",
      :server_url => "https://muztochka.com/notify"
    ) if @promo_item.present?
  end

  def notify
    @promo_item = PromoItem.find(params[:id])
  end

  private

    def promo_item_params
      params.require(:promo_item).permit(:product_id, :promo_service_id)
    end
end
