class SessionsController < Devise::SessionsController
  respond_to :js
  layout false
  def create
    self.resource = warden.authenticate(auth_options)

    if resource && resource.active_for_authentication?
      sign_in(resource_name, resource)
    end
  end

end