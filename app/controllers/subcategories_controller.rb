class SubcategoriesController < ApplicationController

  def show
    @subcategory = Subcategory.friendly.find(params[:id])
    @category = @subcategory.category

    @categories = Category.all
    @brands = Brand.approved.find_by_sql("SELECT brands.*, COUNT(products.id) AS c FROM brands, products WHERE products.brand_id = brands.id GROUP BY brands.id ORDER BY c DESC").take(12)
    prepare_products

    if params[:category].present?
      @promo = @category.products.joins(:promo_items).where('promo_items.status = ? AND promo_items.view_count > ?', 'success', 0).sample(3)
    elsif params[:subcategory].present?
      @promo = @category.products.joins(:promo_items).where('promo_items.status = ? AND promo_items.view_count > ?', 'success', 0).where(:subcategory_id => params[:subcategory]).sample(3)
    else
      @promo = @category.products.joins(:promo_items).where('promo_items.status = ? AND promo_items.view_count > ?', 'success', 0).sample(3)
    end

    set_seo(@subcategory)
  end

  def prepare_products
    limit = 16
    @page = params[:page] ? params[:page].to_i : 1
    if params[:search]
      relation = @subcategory.products.search(params[:search])
    else
      relation = @subcategory.products.all
    end
    unless params[:detail].blank?
      relation = relation.where(:detail_id => params[:detail])
    end
    unless params[:brands].blank?
      relation = relation.where(:brand_id => params[:brands])
    end
    unless params[:min_year].blank?
      relation = relation.where('year >= ?', params[:min_year].to_i)
    end
    unless params[:max_year].blank?
      relation = relation.where('year <= ?', params[:max_year].to_i)
    end
    if params[:main_filter] == "handmade"
      relation = relation.where('condition != ? OR handmade = ?', "Новое", true)
    end
    if params[:main_filter] == "new"
      relation = relation.where(:condition => "Новое")
    end
    @products = relation.limit(limit).offset((@page - 1)*limit)
    count = relation.count
    @pages = count.modulo(limit) == 0 ? count/limit : count.div(limit) + 1
  end

end
