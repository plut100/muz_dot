class SubscribersController < ApplicationController
  def create
    @subscriber = Subscriber.new(subscriber_params)
    respond_to do |format|
      if @subscriber.save
        format.js { render 'success', layout: false }
      else
        format.js { render 'error', layout: false  }
      end
    end
  end

private

  def subscriber_params
    params.require(:subscriber).permit(:email)
  end
end
