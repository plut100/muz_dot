class UsersController < ApplicationController
  before_action :set_user, :only => [:show, :edit, :set_password]

  def show
    @categories = Category.all
    @brand = Brand.approved
  end

  def edit
  end

  def set_password
    respond_to do |format|
      if current_user.update(password_params)
        # Sign in the user by passing validation in case their password changed
        sign_in current_user, :bypass => true
        format.js {render :js => "window.location.href='"+root_path+"'"}
      else
        format.js { render 'error', layout: false }
      end
    end
  end

  def update
    respond_to do |format|
      if params[:user][:avatar]
        if current_user.update(user_avatar_params)
          flash[:notice] = 'Информация обновлена'
          format.html { render action: 'edit' }
          format.js { render 'success_edit', layout: false }
        else
          flash[:notice] = 'Информация не обновлена'
          format.html { render action: 'edit' }
          format.js { render 'error', layout: false }
        end

      elsif params[:user][:email] or params[:user][:name] or params[:user][:surname]
        if current_user.update(user_nopassword_params)
          flash[:notice] = 'Информация обновлена'
          format.html { render action: 'edit' }
          format.js { render 'success_edit', layout: false }
        else
          flash[:notice] = 'Информация не обновлена'
          format.html { render action: 'edit' }
          format.js { render 'error', layout: false }
        end

      elsif params[:user][:current_password] or params[:user][:password]
        if current_user.update_with_password(user_password_params) && !params[:user][:password].blank?
          flash[:notice] = 'Информация обновлена'
          format.html { render action: 'edit' }
          format.js { render 'success_edit', layout: false }
        else
          current_user.errors.add(:password, "Пароль недостаточной длины (не может быть меньше 8 символов)") if params[:user][:password].blank?
          flash[:notice] = 'Информация не обновлена'
          format.html { render action: 'edit' }
          format.js { render 'error_edit', layout: false }
        end
      end
    end
  end

  def create
    @user = User.new(user_params)
    respond_to do |format|
      if @user.save
        format.html { redirect_to root_path, notice: 'User was successfully created.' }
        format.js { render 'success_creation', layout: false }
      else
        format.html { redirect_to root_path }
        format.js { render 'error', layout: false }
      end
    end
  end


  def favorites
    limit = 8
    @page = params[:page] ? params[:page].to_i : 1
    relation = Favorite.where(user: current_user).joins(:product).where('products.status = ?', true)
    @favorites = relation.limit(limit).offset((@page - 1)*limit)
    count = relation.count
    @pages = count.modulo(limit) == 0 ? count/limit : count.div(limit) + 1
  end

  def goods
    limit = 8
    @page = params[:page] ? params[:page].to_i : 1
    if params[:q]
      relation = Product.unscoped.of_owner.active.where(:shop => current_user.shop).includes(:promo_items).includes(:photos).order('created_at DESC').search(params[:q])
    else
      relation = Product.unscoped.of_owner.where(:shop => current_user.shop).includes(:promo_items).includes(:photos).order('created_at DESC')
    end

    if params[:type] == 'non-active'
      relation = relation.non_active
    elsif params[:type] == 'active' || params[:type] == 'all'
      relation = relation.active
    end

    @products = relation.limit(limit).offset((@page - 1)*limit)
    count = relation.count
    @pages = count.modulo(limit) == 0 ? count/limit : count.div(limit) + 1

    @promos = PromoService.all
    @promo_item = PromoItem.new
    @product_deletion_reasons = ProductDeletionReason.all
  end

  def destroy_social_account
    @account = SocialAccount.find(params[:account_id])
    @account.destroy
    redirect_to :back
  end

  def delete_avatar
    @user = current_user
    @user.avatar.destroy
    @user.avatar.clear
    @user.save
    redirect_to edit_user_path(@user)
  end

  private

  def set_user
    @user = current_user
  end

  def password_params
    params.require(:user).permit(:password, :password_confirmation)
  end

  def user_params
    params.require(:user).permit(:name, :surname, :email, :current_password, :password, :password_confirmation, :avatar)
  end

  def user_avatar_params
    params.require(:user).permit(:avatar)
  end

  def user_password_params
    params.require(:user).permit(:current_password, :password)
  end

  def user_nopassword_params
    params.require(:user).permit(:name, :surname, :email)
  end

end

