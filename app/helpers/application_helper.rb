module ApplicationHelper
  def goods_sort_type_to_text type
    case type
      when 'all'        then 'Смотреть все'
      when 'non-active' then 'Смотерть не активные'
      when 'active'     then 'Смотерть активные'
      else 'Смотреть все'
    end
  end
end
