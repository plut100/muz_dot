class Mailer < ActionMailer::Base
  default from: "noreply@muztochka.com",
          subject: "Муzточка"

  def feedback_letter_send(feedback)
    @feedback = feedback
    mail(:to => "muztochka.com@gmail.com", :subject => "Новое сообщение") do |format|
      format.html { render 'new_feedback' }
    end
  end

end
