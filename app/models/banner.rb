class Banner < ActiveRecord::Base

  default_scope { where(:published => true) }

  has_attached_file :image, :styles => { :big => "700x260#", :medium => "460x120#", :small => "220x120#", :product_banner => "300x400#" }
  validates_attachment :image,
                        content_type: { content_type: /^image\/(jpeg|jpg|png|gif)$/ },
                        size: { in: 0..10.megabytes }
end
