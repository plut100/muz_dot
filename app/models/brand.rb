class Brand < ActiveRecord::Base
  before_save :set_seo

  has_many :products, dependent: :destroy
  validates :title, uniqueness: true

  scope :approved, -> { where(:approved => true) }
  scope :alpha_order, -> { order(:title => :asc) }

  has_attached_file :image, :styles => { :large => "1920x220#", :thumb => "284x44#" },
                            default_style: :original, :default_url =>  "bg/bg_filter.jpg"
  validates_attachment :image,
                        content_type: { content_type: /^image\/(jpeg|jpg|png|gif)$/ },
                        size: { in: 0..5.megabytes }


  extend FriendlyId
  friendly_id :title, use: :slugged

  def initial
    return '?' if title.blank?
    title.slice(0).chr.upcase
  end

  def self.alpha(query)
    where("brands.title ILike ?", "#{query}%")
  end

  def self.with_products
    includes(:products).where.not(products: { id: nil })
  end

  def should_generate_new_friendly_id?
    slug.blank? || slug_changed?
  end

  def set_seo
    if self.seo_title.blank?
      self.seo_title = self.title + " купить по цене от 50$ на muztochka.com"
    end

    if self.seo_description.blank?
      self.seo_description = "Хочешь купить " + self.title + "?" + " " + "Покупай на Музточке: б/у и новые " + self.title + ". Muztochka.com - товары напрямую от продавцов. Выгодные цены. Удобный сервис."
    end
  end

end
