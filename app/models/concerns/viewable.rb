module Viewable
  extend ActiveSupport::Concern

  included do
    is_impressionable :counter_cache => true, :unique => :session_hash, :column_name => :views_count
  end
end
