class Conversation < ActiveRecord::Base
  default_scope { order(:updated_at) }

  belongs_to :sender, class_name: 'User'
  belongs_to :recipient, class_name: 'User'

  has_many :messages

  before_create :set_read_at_defaults

  validates :sender, :recipient, presence: true
  validates :sender, uniqueness: { scope: :recipient }

  def user_read_at(user_id)
    if self.sender_id == user_id
      self.sender_read_at
    else
      self.recipient_read_at
    end
  end

  def count_unread(user_id)
    Message.where(:conversation_id => self.id)
           .where('created_at > ?', user_read_at(user_id))
           .where('sender_id != ?', user_id)
           .count
  end

  def sender?(user_id)
    self.sender_id == user_id
  end

  def self.find_or_create(sender_id, recipient_id)
    @conversation = nil
    unless sender_id == recipient_id
      @conversation = Conversation.where('(sender_id = :sender_id AND recipient_id = :recipient_id) OR (sender_id = :recipient_id AND recipient_id = :sender_id)', sender_id: sender_id, recipient_id: recipient_id).first
      unless @conversation
        @conversation = Conversation.create(
          :sender_id => sender_id,
          :recipient_id => recipient_id
        )
      end
    end
    @conversation
  end

  def companion_name(user)
    if sender == user
      recipient.name
    else
      sender.name
    end
  end

  def update_check(user)
    if sender?(user)
      self.update_attribute(:sender_read_at, Time.zone.now)
    else
      self.update_attribute(:recipient_read_at, Time.zone.now)
    end
  end


private

  def set_read_at_defaults
    self.sender_read_at = Time.zone.now
    self.recipient_read_at = Time.zone.now
  end

end
