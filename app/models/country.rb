class Country < ActiveRecord::Base
  before_save :set_permalink
  before_save :set_seo

  validates :name, presence: true
  validates :name, uniqueness: true

  has_many :cities

  has_attached_file :image, :styles => { :large => "1920x220#", :thumb => "284x44#" },
                            default_style: :original, :default_url =>  "bg/bg_filter.jpg"
  validates_attachment :image,
                        content_type: { content_type: /^image\/(jpeg|jpg|png|gif)$/ },
                        size: { in: 0..5.megabytes }

  default_scope { order('name DESC') }


  def set_permalink
    self.permalink = self.name.parameterize
    self.seo_title = self.name
  end

  def set_seo
    if self.seo_title.blank?
      self.seo_title = "Музыкальные товары в стране " + self.name + " - купить недорого на Муzточка"
    end
  end
end
