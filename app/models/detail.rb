class Detail < ActiveRecord::Base
  belongs_to :category
  has_many :products

  validates :title, presence: true
  validates :category, presence: true

  default_scope { order('position') }
end
