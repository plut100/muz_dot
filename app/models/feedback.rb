class Feedback < ActiveRecord::Base
  after_create :send_mail

  def send_mail
    Mailer.feedback_letter_send(self).deliver
  end
end
