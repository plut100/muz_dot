class Message < ActiveRecord::Base
  default_scope { order('created_at DESC') }

  belongs_to :sender, class_name: 'User'
  belongs_to :conversation, touch: true

  validates :sender, :conversation, presence: true

  after_create :send_email_notification

  def date_string
    self.created_at.strftime('%Y-%m-%d')
  end

  def unreaded?(user)
    user != sender && self.conversation.recipient_read_at < self.created_at
  end

  private

  def send_email_notification
   MessageReceivedEvent.notify_user self.conversation_id, self.sender_id, self.conversation.recipient_id
   MessageSentEvent.notify_user self.conversation_id, self.sender_id, self.conversation.recipient_id
  end

end
