class Photo < ActiveRecord::Base
  belongs_to :product

  default_scope { order('position ASC') }

  has_attached_file :image, :styles => { :large => "220x", :promo => "220x220#", :medium => "110x110#", :thumb => "180x140#", :similar => "140x140#" }
  validates_attachment :image,
                        content_type: { content_type: /^image\/(jpeg|jpg|png|gif)$/ },
                        size: { in: 0..5.megabytes }

end
