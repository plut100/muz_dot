class ProductDeletionReason < ActiveRecord::Base
  validates_uniqueness_of :name_ru, case_sensitive: false, allow_blank: false
end
