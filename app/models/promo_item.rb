class PromoItem < ActiveRecord::Base
	belongs_to :product
	belongs_to :promo_service

	STATUSES = ["pending",     #just created, no payment info
							"success",     #payment successfullt completed, promo active
							"finished" ]   #payment successfullt completed,  all views completed, promo disabled

	scope :payed, -> { where(status: 'success')}
	scope :active, -> { where('view_count > 0')}

	scope :wait_accept, -> { where(status: 'wait_accept')}
	scope :processing, -> { where(status: 'processing')}
	scope :wait_secure, -> { where(status: 'wait_secure')}
	scope :failure, -> { where(status: 'failure')}
	scope :success, -> { where(status: 'success')}

	def decrease_count
		update_attribute(:view_count, view_count - 1)
	end

	def self.payment_statuses
		["processing", "success", "failure", "wait_accept", "wait_secure"]
	end
end
