class Shop < ActiveRecord::Base
  belongs_to :user

  extend FriendlyId
  friendly_id :title, use: :slugged

  validates :title, presence: true
  validates :title, length: { minimum: 3, maximum: 30 }
  # validates :title, uniqueness: true

  validates :site, format: {with: /\A[A-Za-z0-9._%+-]+\.[A-Za-z]+\z/, allow_blank: true}

  has_and_belongs_to_many :payment_types
  accepts_nested_attributes_for :payment_types

  has_many :products, dependent: :destroy

  has_attached_file :cover, :styles => { :medium => "940x200#" }, :default_url =>  "bg/bg_main_pic.png"
  validates_attachment :cover,
                              content_type: { content_type: /^image\/(jpeg|jpg|png|gif)$/ },
                              size: { in: 0..5.megabytes }


  def should_generate_new_friendly_id?
    slug.blank? || title_changed?
  end
  
  def count_of_products
    products.count
  end
end
