class MessageReceivedEvent

  def self.notify_user conversation_id, sender_id, recipient_id, time=DateTime.now
    conversation = Conversation.find(conversation_id)
    last_read_time = conversation.recipient_read_at.strftime('%Y-%m-%d %H:%M')
    current_time = Time.now.strftime('%Y-%m-%d %H:%M')
    if conversation.messages.count > 1
      if TimeDifference.between(current_time,last_read_time).in_minutes > 5
        EmailTriggerWorker.perform_in(time, recipient_id, :message_received, nil, sender_id, conversation_id)
      end
    else
      EmailTriggerWorker.perform_in(time, recipient_id, :message_received, nil, sender_id, conversation_id)
    end
  end

  true

end
