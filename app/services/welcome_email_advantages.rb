class WelcomeEmailAdvantages

  def self.notify_user user_id, time=DateTime.now
    EmailTriggerWorker.perform_in(time, user_id, :advantages)
  end

  true

end
