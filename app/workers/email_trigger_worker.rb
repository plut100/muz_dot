class EmailTriggerWorker
  include Sidekiq::Worker

  sidekiq_options retry: 5, queue: :email_triggers

  def perform(user_id, type, fav_id=nil, shop_id=nil, conversation_id=nil)
    user = User.find(user_id)
    email = user.email
    case type.to_sym
      when :advantages
        EmailTriggersMailer.welcome_email_advantages(email).deliver
      when :products
        EmailTriggersMailer.welcome_email_products(email, user_id).deliver
      when :configure
        slug = user.shop.slug
        EmailTriggersMailer.welcome_email_configure(email, slug, user_id).deliver
      when :message_sent
        user_shop = User.find(shop_id)
        shop_name = user_shop.name
        shop_slug = user_shop.shop.slug
        EmailTriggersMailer.message_sent_email(email, shop_name, shop_slug).deliver
      when :message_received
        sender = User.find(shop_id)
        sender_shop_name = sender.name
        sender_shop_slug = sender.shop.slug
        EmailTriggersMailer.message_received_email(email, sender_shop_name, sender_shop_slug, conversation_id).deliver
      when :favorites
        favorite = Favorite.find(fav_id)
        product_title = favorite.product.title
        product_slug = favorite.product.slug
        product_owner_id = favorite.product.shop.user.id
        EmailTriggersMailer.favorites_add_email(email, product_title, product_slug, product_owner_id).deliver
      else
        raise NotImplementedError.new
    end
  end

end
