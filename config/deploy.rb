lock '3.4.0'

set :repo_url,             'git@bitbucket.org:digitalcrafters/muz_dot.git'
set :application,          'muz_dot'
set :user,                 'deploy'
set :linked_dirs,          fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system', 'public/assets')
set :linked_files,         fetch(:linked_files, []).push('config/database.yml', 'config/application.yml')
set :forward_agent,        true
set :whenever_identifier,  -> { "#{fetch(:application)}_#{fetch(:stage)}" }
set :pty,                  true

namespace :deploy do
  task :start do
    on roles(:app) do
      execute :sudo, :service, 'nginx',     'start'
      execute :sudo, :service, 'muztochka', 'start'
    end
  end

  task :stop do
    on roles(:app) do
      execute :sudo, :service, 'nginx',     'stop'
      execute :sudo, :service, 'muztochka', 'stop'
    end
  end

  task :restart do
    on roles(:app) do
      execute :sudo, :service, 'nginx',     'restart'
      execute :sudo, :service, 'muztochka', 'restart'
    end
  end

  after :published, :notify do
    invoke "robots:symlink"
    invoke "puma:symlink"
    invoke "nginx:symlink"
    invoke "initscript:install"
    invoke "logrotate:install"
    invoke "deploy:restart"
  end

end
