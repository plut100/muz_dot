set :rails_env,    :production
set :application,  'muz_dot'

server 'prod.muztochka.com',
  user:   'deploy',
  roles:  %w(app web db),
  ssh_options: {
    forward_agent:  true,
    port:           2000
  }

set :deploy_to,        "/home/deploy/app/"
set :nginx_conf_path,  '/home/deploy/conf/'

namespace :deploy do
  after :publishing, 'sitemap:refresh'
end

