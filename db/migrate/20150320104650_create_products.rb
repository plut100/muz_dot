class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :title
      t.text :description
      t.string :mark
      t.integer :year
      t.string :color
      t.decimal :price, precision: 8, scale: 2
      t.string :condition
      t.boolean :handmade, default: false
      t.boolean :status, default: true
      t.string :video

      t.string :slug

      t.integer :views_count, default: 0

      t.string :formatted_address
      t.string :country
      t.string :state
      t.string :city

      t.float :lat
      t.float :lng

      t.string :country_producing

      t.references :category
      t.references :subcategory
      t.references :detail
      t.references :brand
      t.references :shop

      t.timestamps
    end
  end
end
