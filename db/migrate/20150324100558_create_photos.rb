class CreatePhotos < ActiveRecord::Migration
  def change
    create_table :photos do |t|
      t.attachment :image
      t.references :product
      t.integer :position

      t.timestamps
    end
  end
end
