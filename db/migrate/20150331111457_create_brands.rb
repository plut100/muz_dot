class CreateBrands < ActiveRecord::Migration
  def change
    create_table :brands do |t|
      t.string :title
      t.boolean :approved, default: false

      t.string :slug

      t.timestamps
    end
  end
end
