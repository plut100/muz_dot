class CreateBanners < ActiveRecord::Migration
  def change
    create_table :banners do |t|
      t.string :title
      t.attachment :image
      t.string :link
      t.boolean :published
      t.integer :position

      t.timestamps
    end
  end
end
