class CreateConversations < ActiveRecord::Migration
  def change
    create_table :conversations do |t|
      t.datetime :sender_read_at, :recipient_read_at

      t.references :sender, null: false, index: true
      t.references :recipient, null: false, index: true

      t.timestamps
    end
  end
end
