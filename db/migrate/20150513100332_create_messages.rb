class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.text :text

      t.references :conversation, null: false, index: true
      t.references :sender, null: false, index: true

      t.timestamps
    end
  end
end
