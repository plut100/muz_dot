class AddSeoToAll < ActiveRecord::Migration
  def change
    add_column :categories, :seo_title, :string
    add_column :categories, :seo_description, :text

    add_column :subcategories, :seo_title, :string
    add_column :subcategories, :seo_description, :text

    add_column :products, :seo_title, :string
    add_column :products, :seo_description, :text

    add_column :contents, :seo_title, :string
    add_column :contents, :seo_description, :text

    add_column :brands, :seo_title, :string
    add_column :brands, :seo_description, :text
  end
end
