class UpdateSeedsSubcategoryKlavishnye < ActiveRecord::Migration
  def up
    subcategory_klavishnye_seo_description = "UPDATE subcategories SET seo_description = REPLACE(seo_description, 'Клависин', 'Клавесин');"
    execute subcategory_klavishnye_seo_description

    subcategory_klavishnye_seo_title = "UPDATE subcategories SET seo_title = REPLACE(seo_description, 'Клависин', 'Клавесин');"
    execute subcategory_klavishnye_seo_title
  end
end
