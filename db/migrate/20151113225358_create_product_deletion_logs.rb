class CreateProductDeletionLogs < ActiveRecord::Migration
  def change
    create_table :product_deletion_logs do |t|
      t.references :user, index: true
      t.references :reason, index: true
      t.references :product, index: true
      t.timestamps
    end
  end
end
