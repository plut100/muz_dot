class AddIsReuploadToPhotos < ActiveRecord::Migration
  def change
    add_column :photos, :is_reupload, :boolean, default:false, nil: false
  end
end
