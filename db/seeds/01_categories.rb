print 'Categories.......'

categories_titles = ['Электрогитары', 'Педали & Эффекты', 'Усилители', 'Акустические гитары',
                      'Аксессуары & Разное', 'Бас-гитары', 'Ударные & Перкуссия',
                      'Духовые', 'Смычковые', 'Щипковые', 'Народные инструменты', 'Клавишные',
                      'DJ оборудование', 'Звук', 'Свет & Шоу', 'Другое']

categories = (0..15).to_a.map do |i|
  Category.create({
    title: categories_titles[i % categories_titles.length],
    position: i+1
    })
end

puts "*** OK *********\n\n"
