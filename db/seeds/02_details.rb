print 'Details.......'

        guitar_details = ['Бриджи', 'Грифы', 'Звукосниматели', 'Кнопки, тумблеры',
                          'Колки', 'Корпусы', 'Кофры, кейсы, чехлы', 'Лады', 'Пикгарды',
                          'Потенциометры', 'Ремни', 'Стойки, стенды','Стреплоки, винты',
                          'Струнодержатели', 'Струны', 'Другие']

        pedals_details = ['Блоки питания', 'Кабель', 'Кнопки-переключатели', 'Корпусы',
                          'Педалборды', 'Переходники', 'Потенциометры', 'Другие']

    amplifiers_details = ['Аттенюаторы', 'Динамики', 'Лампы', 'Покрытие для корпусов',
                          'Радиодетали', 'Ткань для гриля', 'Уголки, ленты, винты',
                          'Футсвичи', 'Чехлы', 'Шильдики', 'Другие']

      acoustic_details = ['Грифы', 'Звукосниматели', 'Колки', 'Корпусы', 'Кофры, кейсы, чехлы',
                          'Лады', 'Подставки под ногу', 'Порожки', 'Стойки, стенды',
                          'Струнодержатели', 'Струны', 'Другие']



          bass_details = ['Бриджи', 'Грифы', 'Звукосниматели', 'Кнопки, тумблеры', 'Колки',
                          'Корпусы, дэки', 'Кофры, кейсы, чехлы', 'Лады', 'Пикгарды', 'Потенциометры',
                          'Ремни', 'Стойки, стенды', 'Стреплоки, винты', 'Струнодержатели', 'Струны',
                          'Другие']

    percussion_details = ['Датчики и измерители', 'Кожа и пластики', 'Крепления и переходники',
                          'Палочки, щетки, руты', 'Педали', 'Стойки, рампы, клэмпы', 'Стулья',
                          'Сумки, кейсы, чехлы', 'Другие']

          wind_details = ['Клапан', 'Лигатура', 'Мундштук', 'Раструб', 'Ремень', 'Смазка',
                          'Тростина', 'Чехол, кофр, футляр', 'Шейка', 'Другие']

           bow_details = ['Грифы', 'Деки', 'Держатели', 'Колки', 'Подбородники', 'Смычки',
                          'Струнодержатели', 'Струны', 'Другие']

       plucked_details = ['Грифы', 'Деки', 'Колки', 'Струнодержатели', 'Струны', 'Другие']

   traditional_details = ['Грифы', 'Деки','Струнодержатели', 'Струны', 'Другие']

     keyboards_details = ['Педали', 'Подставки', 'Струны', 'Стулья и скамейки', 'Другие']

            dj_details = ['Иглы', 'Картриджи для игл', 'Кейсы, сумки', 'Пластинки с тайм-кодом',
                          'Стойки, подставки', 'Другие']

         sound_details = ['Кейсы, чехлы', 'Крепления', 'Разъемные планки', 'Ремкомплект для динамиков',
                          'Рупоры', 'Ручки, замки и колеса', 'Рэки', 'Сетки для акстических систем',
                          'Стойки акустические', 'Стойки микрофонные', 'Уголки для корпусов', 'Другие']

   light_shows_details = ['Ароматические добавки', 'Газ для генераторов огня', 'Жидкость для дыма',
                          'Жидкость для мыльных пузырей', 'Жидкость для снега', 'Жидкось для пены',
                          'Конфетти', 'Лампы', 'Стойки', 'Струбцины', 'Ультро-фиолетовая жидкость',
                          'Другие']



12.times do |i|
  Detail.create({
    title: guitar_details[i % guitar_details.length],
    position: i+1,
    category_id: 1
    })
end

8.times do |i|
  Detail.create({
    title: pedals_details[i % pedals_details.length],
    position: i+1,
    category_id: 2
    })
end

11.times do |i|
  Detail.create({
    title: amplifiers_details[i % amplifiers_details.length],
    position: i+1,
    category_id: 3
    })
end

12.times do |i|
  Detail.create({
    title: acoustic_details[i % acoustic_details.length],
    position: i+1,
    category_id: 4
    })
end

12.times do |i|
  Detail.create({
    title: bass_details[i % bass_details.length],
    position: i+1,
    category_id: 6
    })
end

9.times do |i|
  Detail.create({
    title: percussion_details[i % percussion_details.length],
    position: i+1,
    category_id: 7
    })
end

10.times do |i|
  Detail.create({
    title: wind_details[i % wind_details.length],
    position: i+1,
    category_id: 8
    })
end

9.times do |i|
  Detail.create({
    title: bow_details[i % bow_details.length],
    position: i+1,
    category_id: 9
    })
end

6.times do |i|
  Detail.create({
    title: plucked_details[i % plucked_details.length],
    position: i+1,
    category_id: 10
    })
end

5.times do |i|
  Detail.create({
    title: traditional_details[i % traditional_details.length],
    position: i+1,
    category_id: 11
    })
end

5.times do |i|
  Detail.create({
    title: keyboards_details[i % keyboards_details.length],
    position: i+1,
    category_id: 12
    })
end

6.times do |i|
  Detail.create({
    title: dj_details[i % dj_details.length],
    position: i+1,
    category_id: 13
    })
end

12.times do |i|
  Detail.create({
    title: sound_details[i % sound_details.length],
    position: i+1,
    category_id: 14
    })
end

12.times do |i|
  Detail.create({
    title: light_shows_details[i % light_shows_details.length],
    position: i+1,
    category_id: 15
    })
end

puts "*** OK *********\n\n"
