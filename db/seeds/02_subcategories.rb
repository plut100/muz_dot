print 'Subcategories.......'

         guitars_sub_titles = ['12-ти струнные', 'Aрктоп', 'Баритон', 'Левосторонние', 'Лэпстил',
                               'Миди гитары', 'Полупустой корпус', 'Пустотелый корпус', 'Цельный корпус']

          pedals_sub_titles = ['Fuzz', 'Loop-станции', 'Noise-генераторы', 'Ring-модуляторы', 'Wah-Wah',
                               'Басовые педали', 'Дилэй', 'Директ-боксы', 'Дисторшн', 'Компрессоры',
                               'Контроллеры и селекторы', 'Овердрайв и буст', 'Октаверы', 'Процессоры',
                               'Реверб', 'Тремоло и вибрато', 'Тюннеры', 'Фленжеры', 'Футсвичи',
                               'Фэйзеры', 'Хорус', 'Эквалайзеры']

      amplifiers_sub_titles = ['Акустические', 'Бас головы', 'Бас кабинеты', 'Бас комбики', 'Гитарные головы',
                               'Гитарные комбики', 'Кабинеты', 'Клавишные']

acoustic_guitars_sub_titles = ['12-ти струнные', '7-ми струнные', 'Дóбро', 'Джамбо', 'Дредноут', 'Классические',
                               'Концертные', 'Левосторонние', 'Уменьшенный корпус', 'Фолк', 'Электро-акустические']

     accessories_sub_titles = ['iPhone / iPad оборудование', 'Адаптеры и блоки питания',  'Атрибутика',
                               'Кабель', 'Камертон', 'Каподастры и слайдеры', 'Ключи', 'Медиаторы',
                               'Метрономы', 'Музыкальное ПО', 'Ноты, книги и DVD', 'Пюпитры, подставки',
                               'Средства ухода', 'Сувениры', 'Тюнеры', 'Другие']

    bass_guitars_sub_titles = ['4-х струнные', '5-ти струнные', '6-ти струнные', 'Акустические',
                               'Короткая мензура', 'Левосторонние', 'Цельный корпус']

      percussion_sub_titles = ['Бочки', 'Джембе', 'Драм-машины', 'Кахоны', 'Конга и бонго', 'Малые барабаны',
                               'Наборы барабанов', 'Наборы тарелок', 'Тарелки', 'Томы', 'Ударные установки',
                               'Шейкеры', 'Электронные установки', 'Другие']

            wind_sub_titles = ['Балабаны', 'Блокфлейты', 'Волторны', 'Гобойи', 'Губные гармоники', 'Зурны',
                               'Кларнеты', 'Корнеты', 'Панфлейты', 'Саксофоны', 'Тромбоны', 'Трубы', 'Тубы',
                               'Фаготы', 'Флейты', 'Другие']

             bow_sub_titles = ['Виолончели', 'Контрабасы', 'Скрипки', 'Альты', 'Смычки', 'Струны', 'Детали']

         plucked_sub_titles = ['Арфы', 'Балалайки', 'Гусли', 'Домбры', 'Домры', 'Жетыгены', 'Кото', 'Лютни',
                               'Тары', 'Цитры', 'Чаранги', 'Другие']

     traditional_sub_titles = ['Банджо', 'Бандуры', 'Варган', 'Губные гармошки', 'Гудки', 'Кобзы', 'Кобызы',
                               'Ложки', 'Мандолины', 'Никельхарпы', 'Трембиты', 'Укулеле', 'Хардангерфеле',
                               'Цимбалы', 'Другие']

       keyboards_sub_titles = ['MIDI клавиатуры', 'Аккордеоны', 'Аналоговые синтезаторы', 'Баян', 'Клавесин',
                               'Контроллеры', 'Органы', 'Рабочие станции', 'Рояли', 'Синтезаторы', 'Фортепиано',
                               'Цифровые пианино']

              dj_sub_titles = ['Проигрыватели', 'Пульты', 'Вертушки', 'Микшеры', 'Готовые системы',
                               'Интерфейсы', 'Эффекторы', 'Картриджи для игл', 'Иглы']

           sound_sub_titles = ['Акустические системы, мониторы' ,'Аудиоинтерфейсы', 'Динамики ВЧ',
                               'Динамики НЧ (сабвуферы)', 'Динамики СЧ', 'Звуковые карты', 'Караоке системы',
                               'Микрофоны', 'Микшерные пульты', 'Наушники', 'Плагины', 'Процессоры',
                               'Радиосистемы', 'Рекордеры','Усилители','Другие']

     light_shows_sub_titles = ['Головы', 'Сканеры', 'Заливочный свет', 'Световые эффекты',
                               'Следящие прожекторы', 'Светильники', 'LED подсветка',
                               'Стробоскопы', 'Лазеры', 'Дым машины', 'Снег машины',
                               'Мыльные пузыри', 'Лампы накаливания', 'Диммеры и рэле',
                               'Светодиодная лента', 'Расходные материалы', 'Детали']


9.times do |i|
  Subcategory.create({
    title: guitars_sub_titles[i % guitars_sub_titles.length],
    position: i+1,
    category_id: 1
    })
end

22.times do |i|
  Subcategory.create({
    title: pedals_sub_titles[i % pedals_sub_titles.length],
    position: i+1,
    category_id: 2
    })
end

8.times do |i|
  Subcategory.create({
    title: amplifiers_sub_titles[i % amplifiers_sub_titles.length],
    position: i+1,
    category_id: 3
    })
end

11.times do |i|
  Subcategory.create({
    title: acoustic_guitars_sub_titles[i % acoustic_guitars_sub_titles.length],
    position: i+1,
    category_id: 4
    })
end

16.times do |i|
  Subcategory.create({
    title: accessories_sub_titles[i % accessories_sub_titles.length],
    position: i+1,
    category_id: 5
    })
end


7.times do |i|
  Subcategory.create({
    title: bass_guitars_sub_titles[i % bass_guitars_sub_titles.length],
    position: i+1,
    category_id: 6
    })
end

14.times do |i|
  Subcategory.create({
    title: percussion_sub_titles[i % percussion_sub_titles.length],
    position: i+1,
    category_id: 7
    })
end


16.times do |i|
  Subcategory.create({
    title: wind_sub_titles[i % wind_sub_titles.length],
    position: i+1,
    category_id: 8
    })
end

7.times do |i|
  Subcategory.create({
    title: bow_sub_titles[i % bow_sub_titles.length],
    position: i+1,
    category_id: 9
    })
end

13.times do |i|
  Subcategory.create({
    title: plucked_sub_titles[i % plucked_sub_titles.length],
    position: i+1,
    category_id: 10
    })
end

15.times do |i|
  Subcategory.create({
    title: traditional_sub_titles[i % traditional_sub_titles.length],
    position: i+1,
    category_id: 11
    })
end

12.times do |i|
  Subcategory.create({
    title: keyboards_sub_titles[i % keyboards_sub_titles.length],
    position: i+1,
    category_id: 12
    })
end

9.times do |i|
  Subcategory.create({
    title: dj_sub_titles[i % dj_sub_titles.length],
    position: i+1,
    category_id: 13
    })
end

16.times do |i|
  Subcategory.create({
    title: sound_sub_titles[i % sound_sub_titles.length],
    position: i+1,
    category_id: 14
    })
end

17.times do |i|
  Subcategory.create({
    title: light_shows_sub_titles[i % light_shows_sub_titles.length],
    position: i+1,
    category_id: 15
    })
end

puts "*** OK *********\n\n"
