print 'Products.......'

products_titles = ['Mastiff Distortion', 'Taylor 114E Left-Handed', 'Conn 6505', 'Jupiter 969GL Artist Eb Alto Saxophone', 'Fender Super Reverb', 'Rogers Drum Set-Fullerton', 'Yamaha Recording Custom 5 Piece drum set kit', 'TC Electronics Hall of Fame', 'DW Collectors Series', 'Fender Bass', 'Gibson Les Paul Standard', 'Marshall Silver Jubilee 25/53', 'RainSong BI-PA1000NS', 'Fender Stratocaster', 'Kiso Suzuki W180 Dreadnought Acoustic Guitar', 'Boss DD-3', 'G.E.C. A2900 1948 vintage tube', 'Blueridge BR-40T Tenor 2009 Natural', 'Buffet R-13 2008 Professional Clarinet', 'Premier APK 1990 White 4 PC Shell Kit']
products_marks = ['Distortion', '114E Left-Handed', '6505', '969GL Artist Eb Alto Saxophone', 'Super Reverb', 'Drum Set-Fullerton', 'Recording Custom 5 Piece drum set kit', 'Hall of Fame', 'Collectors Series', 'Bass', 'Les Paul Standard', 'Silver Jubilee 25/53', 'BI-PA1000NS', 'Stratocaster', 'Dreadnought Acoustic Guitar', 'DD-3', 'CA2900 vintage tube', 'BR-40T Tenor', 'R-13 Professional Clarinet', 'APK 4 PC Shell Kit']
products_prices = ['245', '389', '970', '1455', '2200', '1450', '1199', '98', '1400', '2500', '1900', '4300', '355', '1900', '50', '250', '230', '295', '3200', '499']
products_conditions = ['Новое', 'Как новое', 'Отличное', 'Очень хорошее', 'Хорошее', 'Удовлетворительное', 'Плохое', 'Нерабочее']
products_years = ['2011', '2010', '1970', '2001', '1967', '2011', '2009', '2014', '1980', '1989', '2002', '1987', '1983', '2007', '1970', '1978', '1948', '2009', '2008', '1990']
products_countries = ['Германия', 'Беларусь', 'Германия', 'Азербайджан', 'США', 'США', 'США', 'США', 'Беларусь', 'США', 'США', 'США', 'Казахстан', 'США', 'Азербайджан', 'Япония', 'Великобритания', 'Израиль', 'Андорра', 'Казахстан']

Product.create(
  title: 'Gibson ES-330',
  description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet. Duis sagittis ipsum. Praesent mauris. Fusce nec tellus sed augue semper porta. Mauris massa. Vestibulum lacinia arcu eget nulla. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Curabitur sodales ligula in libero. Sed dignissim lacinia nunc. Curabitur tortor. Pellentesque nibh. Aenean quam. In scelerisque sem at dolor. Maecenas mattis. Sed convallis tristique sem. Proin ut ligula vel nunc egestas porttitor. Morbi lectus risus, iaculis vel, suscipit quis, luctus non, massa. Fusce ac turpis quis ligula lacinia aliquet. Mauris ipsum. Nulla metus metus, ullamcorper vel, tincidunt sed, euismod in, nibh.',
  mark: 'ES-330',
  price: '999.99',
  condition: 'Новое',
  category_id: 1,
  subcategory: Subcategory.find_by(:category_id => 1),
  brand_id: 1,
  country_producing: 'США',
  country: 'Украина',
  city: 'Винница',
  year: '1999',
  color: 'Brown',
  handmade: true,
  video: 'https://www.youtube.com/watch?v=9tCP-s0auOk',
  shop_id: 1
  )

Photo.create(
  product_id: 1,
  image: File.open(Rails.root.join('test', 'assets', 'images', 'products', "product-1.jpg")),
  position: 1
  )

j=1
k=1
20.times do |i|
  product = Product.create({
    title: products_titles[i % products_titles.length],
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet. Duis sagittis ipsum. Praesent mauris. Fusce nec tellus sed augue semper porta. Mauris massa. Vestibulum lacinia arcu eget nulla. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Curabitur sodales ligula in libero. Sed dignissim lacinia nunc. Curabitur tortor. Pellentesque nibh. Aenean quam. In scelerisque sem at dolor. Maecenas mattis. Sed convallis tristique sem. Proin ut ligula vel nunc egestas porttitor. Morbi lectus risus, iaculis vel, suscipit quis, luctus non, massa. Fusce ac turpis quis ligula lacinia aliquet. Mauris ipsum. Nulla metus metus, ullamcorper vel, tincidunt sed, euismod in, nibh.',
    mark: products_marks[i % products_marks.length],
    price: products_prices[i % products_prices.length],
    condition: products_conditions[i % products_conditions.length],
    category_id: k,
    subcategory: Subcategory.find_by(:category_id => k),
    year: products_years[i % products_years.length],
    country_producing: products_countries[i % products_countries.length],
    country: 'Украина',
    city: 'Киев',
    color: 'Natural',
    shop_id: 1,
    brand_id: j
    })
  k+=1
  k=1 if k > Category.all.count
  j+=1
  j=1 if j > Brand.all.count
end

20.times do |i|
  Photo.create({
    product_id: i+2,
    image: File.open(Rails.root.join('test', 'assets', 'images', 'products', "product-#{i+2}.jpg")),
    position: 1
  })
end

puts "*** OK *********\n\n"
