print 'Advices.......'

Advices.create(
  title: 'FREE to list!',
  description: %q{<li>Use categories to help buyers find your item.</li><li>Use the Price Guide for description and pricing benchmarks.</li>}
  )

Advices.create(
  title: 'Only pay 3.5% when your item sells',
  description: %q{<li>$1 minimum and a $350 maximum fee.</li><li>Billed at end of the month via credit card.</li><li>Want to upload many listings at once? Ask us about Inventory Management.</li><li>See how the 3.5% fee works.</li>}
  )

Advices.create(
  title: 'Great photos sell faster',
  description: %q{<li>Read our tips for top-notch photos.</li><li>Billed at end of the month via credit card.</li><li>High-resolution photos look better.</li>}
  )

Advices.create(
  title: 'Make your listing rock!',
  description: %q{<li>Check out guide to gear conditions.</li><li>See how to write a full and accurate description.</li>}
  )

puts "*** OK *********\n\n"
