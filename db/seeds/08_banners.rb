print 'Banners.......'

banners_titles = ['Гитары', 'Ударные', 'Духовые', 'Педали', 'Звук', '']

6.times do |i|
  Banner.create({
    title: banners_titles[i % banners_titles.length],
    position: i+1,
    image: File.open(Rails.root.join('test', 'assets', 'images', 'banners', "#{i+1}.png")),
    link: "http://muzdot.coalla.com.ua/"
  })
end

puts "*** OK *********\n\n"
