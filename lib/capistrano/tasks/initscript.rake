namespace :initscript do
  task :install do
    on roles(:web) do
      execute(:sudo, :cp, "#{release_path}/config/custom/init/#{fetch(:stage)}", "/etc/init.d/muztochka")
      execute(:sudo, :chown, "root:root", "/etc/init.d/muztochka")
      execute(:sudo, :chmod, "0755", "/etc/init.d/muztochka")
    end
  end
end
