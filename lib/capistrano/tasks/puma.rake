namespace :puma do
  task :symlink do
    on roles(:web) do
      execute :ln, "-fs",
        "#{release_path}/config/puma.rb.#{fetch(:stage)}",
        "#{release_path}/config/puma.rb"
    end
  end
end
