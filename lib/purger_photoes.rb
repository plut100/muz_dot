class PurgerPhotoes
  def self.call
    photoes = Photo.where(product_id: nil)
    photoes.destroy_all if photoes.any?
  end
end