class S3Uploader
  def self.call
    products = Product.all.where(is_reupload: false)
    puts "---> Products count is #{products.count}"
    products.each do |product|
      if product.photos.where(is_reupload: false).present?
        product.photos.where(is_reupload: false).each do |photo|
          image_path = Dir["#{Rails.root}/public/system/#{photo.image.path}"].first
          puts image_path
          if image_path.present?
            photo.update(image: File.open(image_path))
            photo.update(is_reupload: true)
          end
          image_path = nil
        end
        product.update(is_reupload: true)
      end
    end if products.present?
    puts '---> after script processed pls. restart web server!!! '
  end
end