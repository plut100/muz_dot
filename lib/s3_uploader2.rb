class S3Uploader2
  def self.call
    ### Banner's block
    banners = Banner.all
    puts "---> Banners count is #{banners.count}"
    if banners.present?
      banners.each do |banner|
        if banner.image.present?
          banner_path = Dir["#{Rails.root}/public/system/#{banner.image.path}"].first
          puts banner_path
          if banner_path.present?
            banner.update(image: File.open(banner_path))
          end
          banner_path = nil
        end
      end
      puts 'banners block finished'
    else
      puts 'no banners found'
    end

    puts '\n Continue ? [Y/N]'
    answer = STDIN.gets.chomp
    if answer == 'N'
      return false
    end

    ### Categories's block
    categories = Category.all
    puts "---> Categories count is #{categories.count}"
    if categories.present?
      categories.each do |category|
        if category.image.present?
          category_path = Dir["#{Rails.root}/public/system/#{category.image.path}"].first
          puts category_path
          if category_path.present?
            category.update(image: File.open(category_path))
          end
          category_path = nil
        end
      end
      puts 'categories block finished'
    else
      puts 'no categories found'
    end

    puts '\n Continue ? [Y/N]'
    answer = STDIN.gets.chomp
    if answer == 'N'
      return false
    end

    ### Citie's block
    cities = City.all
    puts "---> Cities count is #{cities.count}"
    if cities.present?
      cities.each do |city|
        if city.image.present?
          city_path = Dir["#{Rails.root}/public/system/#{city.image.path}"].first
          puts city_path
          if city_path.present?
            city.update(image: File.open(city_path))
          end
          city_path = nil
        end
      end
      puts 'cities block finished'
    else
      puts 'no cities found'
    end

    puts '\n Continue ? [Y/N]'
    answer = STDIN.gets.chomp
    if answer == 'N'
      return false
    end

    ### Shop's block
    shops = Shop.all
    puts "---> Shops count is #{shops.count}"
    if shops.present?
      shops.each do |shop|
        if shop.cover.present?
          shop_path = Dir["#{Rails.root}/public/system/#{shop.cover.path}"].first
          puts shop_path
          if shop_path.present?
            shop.update(cover: File.open(shop_path))
          end
          shop_path = nil
        end
      end
      puts 'shops block finished'
    else
      puts 'no shops found'
    end

    puts '\n Continue ? [Y/N]'
    answer = STDIN.gets.chomp
    if answer == 'N'
      return false
    end

    ### Subcategory's block
    subcategories = Subcategory.all
    puts "---> Subcategorys count is #{subcategories.count}"
    if subcategories.present?
      subcategories.each do |subcategory|
        if subcategory.image.present?
          subcategory_path = Dir["#{Rails.root}/public/system/#{subcategory.image.path}"].first
          puts subcategory_path
          if subcategory_path.present?
            subcategory.update(image: File.open(subcategory_path))
          end
          subcategory_path = nil
        end
      end
      puts 'subcategory block finished'
    else
      puts 'no subcategories found'
    end

    puts '\n Continue ? [Y/N]'
    answer = STDIN.gets.chomp
    if answer == 'N'
      return false
    end

    ### Users's block
    users = User.all
    puts "---> Users count is #{users.count}"
    if users.present?
      users.each do |user|
        if user.avatar.present?
          user_path = Dir["#{Rails.root}/public/system/#{user.avatar.path}"].first
          puts user_path
          if user_path.present?
            user.update(avatar: File.open(user_path))
          end
          user_path = nil
        end
      end
      puts 'user block finished'
    else
      puts 'no subcategories found'
    end


=begin
    ### Countrie's block
    countries = Country.all
    puts "---> Countries count is #{countries.count}"
    if countries.present?
      countries.each do |country|
        if country.image.present?
          country_path = Dir["#{Rails.root}/public/system/#{country.image.path}"].first
          puts country_path
          if country_path.present?
            country.update(image: File.open(country_path))
          end
          country_path = nil
        end
      end
      puts 'countries block finished'
    else
      puts 'no countries found'
    end

    puts '\n Continue ? [Y/N]'
    answer = STDIN.gets.chomp
    if answer == 'N'
      return false
    end


    ### Brand's block
    brands = Brand.all
    puts "---> Brands count is #{brands.count}"
    brands.each do |brand|
      if brand.image.present?
        brand_path = Dir["#{Rails.root}/public/system/#{brand.image.path}"].first
        puts brand_path
        if brand_path.present?
          brand.update(image: File.open(brand_path))
        end
        brand_path = nil
      end
    end if brands.present?
=end

    puts '---> after script processed pls. restart web server!!!'
  end
end


