namespace :db do
  desc 'recalculate counter of viewing for Products'
  task recalculate_counter_of_viewing: :environment do

    products = Product.all.to_a
    if products.present?
      products.each do |product|
        unless product.views_count == product.count_of_visits.to_i
          if product.views_count > product.count_of_visits.to_i
            (1..(product.views_count - product.count_of_visits.to_i)).each do |data|
              product.visitors_count.create(remote_ip: data)
            end
          end
        end
      end

    end
  end
end
