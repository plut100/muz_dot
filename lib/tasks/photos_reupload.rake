namespace :db do
  desc 'photos reupload to S3'
  task photos_reupload: :environment do
    S3Uploader.call
  end
end
