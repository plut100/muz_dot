namespace :db do
  desc 'populate ProductDeletionReason by data'
  task populate_product_deletion_reason: :environment do
    ProductDeletionReason.create!(name_ru: 'Продал на Музточке')
    ProductDeletionReason.create!(name_ru: 'Продал в другом месте')
    ProductDeletionReason.create!(name_ru: 'Передумал продавать')
  end
end

